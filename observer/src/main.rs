#[macro_use] extern crate rocket;
extern crate clap;


use configuration::ObserverServiceConfiguration;
use rocket::routes;
use clap::Parser;

mod configuration;
mod routes;

use routes::{inbox, workflows, workflows_status, workflow_status, workers};
use commons::{subscription::{SubscriptionManager,WORKFLOW, WORKFLOWCOMPLETED, WORKFLOWCANCELED, GENERATORCOMMAND,
    GENERATORRESULT, PROVIDERCOMMAND, PROVIDERRESULT, EXECUTIONCOMMAND, EXECUTIONRESULT, EXECUTIONERROR, NOTIFICATION}, events::PendingEventQueue};

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Opts {
    /// alternative context
    #[clap(long)]
    context: Option<String>,
    /// alternative config file
    #[clap(long)]
    config: Option<String>
}

fn load_configuration() -> Result<ObserverServiceConfiguration, String>{
    let opts = Opts::parse();

    let config = match opts.config {
        Some(file) => file,
        None => String::from("config/observer.yaml"),
    };

    let configuration =configuration::ObserverServiceConfiguration::from_file(&config);

    configuration
}

async fn register_service() -> Result<SubscriptionManager, String> {
    let mut mngr = SubscriptionManager::new();
    
    let wanted_evts = vec![    
        WORKFLOW,
        WORKFLOWCOMPLETED,
        WORKFLOWCANCELED,
        GENERATORCOMMAND,
        GENERATORRESULT,
        PROVIDERCOMMAND,
        PROVIDERRESULT,
        EXECUTIONCOMMAND,
        EXECUTIONRESULT,
        EXECUTIONERROR,
        NOTIFICATION,
    ];

    let registration = match mngr.init_subscriptions(wanted_evts).await {
        Ok(_) => {
            mngr.display_active_subscruptions();
            Ok(mngr)
        },
        Err(e) => {
            mngr.release_subscriptions().await?;
            Err(e)
        }
    };

    registration
}

async fn launch_server(configuration: &ObserverServiceConfiguration) -> Result<(),String> {
    println!("Loaded configuration {:?}", configuration);

    let opts = Opts::parse();
    let port = match configuration.port(&opts.context) {
        Ok(p) => p,
        Err(e) => return Err(format!("No valid port configuration. {}", e))
    };

    println!("Will listen on{:?}", port);

    let figment = rocket::Config::figment() .merge(("port", port));
    let routes = routes![inbox, workflows, workflows_status, workflow_status, workers];

    let recorded_evts = PendingEventQueue::new();
    let rocket = rocket::custom(figment)
                                                    .mount("/", routes)
                                                    .manage(recorded_evts);

    let ignited_rocket= match rocket.ignite().await {
        Ok(r) =>r,
        Err(e) => return Err(format!("Something wen't wrong with rocket ignition {}",e))
    };
    
    let flight = match  ignited_rocket.launch().await {
        Ok(_) => Ok(()),
        Err(e) =>  Err(format!("Something wen't wrong with rocket flight {}",e))
    };

    flight
}


async fn cleanup(mut mngr: SubscriptionManager) -> Result<(),String> {
    mngr.release_subscriptions().await
}

#[rocket::main]
async fn main() -> Result<(),String> {

    let conf = match load_configuration() {
        Ok(c) => c,
        Err(e) => return Err(e)
    }; 

    let mngr = match register_service().await {
        Ok(m) => m,
        Err(e) => return Err(e)
    };

    let mut errors = Vec::new();
    if let Err(s) =  launch_server(&conf).await {
        errors.push(s);
    }
    if let Err(c) = cleanup(mngr).await {
        errors.push(c);
    }

    if ! errors.is_empty() {
        return Err(format!("{:?}", errors))
    }

    Ok(())
}