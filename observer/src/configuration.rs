use rocket::{serde::Deserialize};


#[derive(Deserialize, Debug)]
pub struct ObserverServiceConfiguration {

    // #[serde(rename = "apiVersion")]
    // api_version: String, 
    // kind: String, 
    #[serde(rename = "current-context")]
    current_context: String,

    contexts: Vec<Context>,
}

impl ObserverServiceConfiguration {
    
    pub fn from_file(filename: &str) -> Result<ObserverServiceConfiguration, String> {

        let path = std::path::Path::new(filename);
        println!("Will load file {}", path.display());
        let configuration_file = match std::fs::File::open(path) {
            Ok(file) => file,
            Err(e) =>return Err(format!("Could not load file {}. Error: {}", filename,e))
        };
      
        match serde_yaml::from_reader(configuration_file) {
            Ok(config) => return Ok(config),
            Err(e) => return Err(format!("Ill formated configuration file {}.Error: {}", filename,e))
        };
    }

    pub fn port(&self, context: &Option<String>) -> Result<u16, String> {
        let context_value = match context {
            Some(ctx) => ctx,
            None  => &self.current_context,
        };

        match  (&self.contexts).iter().find(|ctx| &ctx.name == context_value) {
         Some(ctx) => Ok(ctx.context.port),
         None => Err(format!("Context {} is invalid ", context_value))
        }
     }
}
#[derive(Deserialize, Debug)]
pub struct Context {
    name: String,
    context: ContextDetail,
}

#[derive(Deserialize, Debug)]
pub struct ContextDetail {
    port: u16,
    // host: String,
    // ssl_context: String,
    // enable_insecure_login: Option<bool>,
    // eventbus: EventbusEnpoint,
    // logfile: Option<String>,
    // trusted_authorities: Option<Vec<String>>
}

// #[derive(Deserialize, Debug)]
// pub struct EventbusEnpoint {
//    endpoint: String,
//    token: String,
// }