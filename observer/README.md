# observer-rust-impl

A (crude) rust implemation of the Open Test Factory observer service [](https://opentestfactory.org/services/observer.html)

## Pre-requisites

The only pre-requisite is to have the [Rust](https://www.rust-lang.org/tools/install) toolchain installed.

This was tested (this ran once) with the following setup

- OS: Debian 11 (bullseye) on Windows 10 via WSL2
  - Kernel  5.10.60.1-microsoft-standard-WSL2

- Rust toolchain
  - Rustup  1.24.3
  - Rustuc 1.57.0
  - Cargo 1.57.0

## Getting started

To run it:

- Just clone the repository

``` shell
git clone https://gitlab.com/florian_gautier/observer-rust-impl
```

- Launch it using cargo

``` shell
cargo run
```

If using the default configuration you'll likely encounter binding issues as it will try to bind 443 (<1024) which is restricted. To evade the issue either:

- Use a different context using the *context* argument. For instance the context *allinone* will bind 7775

``` shell
cargo run -- --context allinone
```

- Use a different otf-formated config file using the *config* argument

``` shell
cargo run -- --config path/to/my-observer-cfg.yaml
```

Et voilà.

## Available endpoints

Following the OpenTestFactory specification the observer exposes five endpoints:

- /inbox (POST)
- /workflows (GET)
- /workflows/status (GET)
- /workflows/{workflow_id}/status (GET)
- /workflows/{workflow_id}/workers (GET)

Currently those are just dummy endpoints...
