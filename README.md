# Rusty Factory

A (crude) rust implemation of some of the Open Test Factory services [](https://opentestfactory.org/services/observer.html)

## Crates

It povides:

- two binaries
  - provider: A rust-written  [Open Test Factory provider plugin](https://opentestfactory.org/specification/plugins.html#provider-plugins) for rust.
  - observer: A crude rusty re-implemeation of the [observer service](https://opentestfactory.org/services/observer.html)

- a *common* library
