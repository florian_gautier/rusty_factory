use rocket::{post, routes, http::Status};


 #[post("/inbox", format="application/json")]
fn inbox() -> Result<Status,()> {
    Ok(Status::Ok)
}

#[rocket::main]
async fn main() {

    let _ = rocket::build().mount("/", routes!(inbox))
                                        .launch().await;

}
