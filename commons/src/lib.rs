extern crate rocket;
extern crate serde;

pub mod events;
pub mod subscription;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}
