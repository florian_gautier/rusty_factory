use std::collections::HashMap;

use reqwest::{header::{USER_AGENT, AUTHORIZATION}, StatusCode};

use rocket::serde::{Serialize, Deserialize};
 
pub const WORKFLOW: &str = "Workflow";
pub const WORKFLOWCOMPLETED: &str = "WorkflowCompleted";
pub const WORKFLOWCANCELED: &str = "WorkflowCanceled";

pub const GENERATORCOMMAND: &str = "GeneratorCommand";
pub const GENERATORRESULT: &str = "GeneratorResult";

pub const PROVIDERCOMMAND: &str = "ProviderCommand";
pub const PROVIDERRESULT: &str = "ProviderResult";

pub const EXECUTIONCOMMAND: &str = "ExecutionCommand";
pub const EXECUTIONRESULT: &str = "ExecutionResult";
pub const EXECUTIONERROR: &str = "ExecutionError";

pub const NOTIFICATION: &str = "Notification";

pub struct SubscriptionManager {
    // Stores the subscriptions uuid for each event {event : uuid}
    subscriptions: HashMap<String,String>
}

impl SubscriptionManager {
    pub fn new() -> Self {
        SubscriptionManager { subscriptions: HashMap::new() }
    }
    pub async fn init_subscriptions(&mut self, events: Vec<&str>) -> Result<(), String> {
    
        let mut first_failed_subscritpion = None;
    
        for evt in events {
            match single_evt_subscription(evt).await {
                Ok(uuid) =>  {self.subscriptions.insert(String::from(evt), uuid);},
                Err(e) => {
                    first_failed_subscritpion= Some((evt,e));
                    break; // no need to proceed further at least a subscription failed
                },
            };
        }
        

        if let Some((evt,err)) = first_failed_subscritpion {
            println!("Failed to register {} - {}", evt, err);

            //at least one subscription failed trying to unregister the one that succeding before aborting
                for (event,uuid) in self.subscriptions.iter() {
                    match unsubscribe_evt(event.as_str(), uuid.as_str()).await {
                        Ok(_) => println!("Unsubscription to event {} succeded", uuid.as_str()),
                        Err(e) => println!("Unsubscription to event {} wen't wrong... {}", uuid.as_str(),e),
                    };
                }

            return Err(format!("Failed to register {} - {}", evt, err))
        }

        Ok(())
    }

    pub fn display_active_subscruptions(&self) {
        println!("{:?}", self.subscriptions);
    }

    pub async fn release_subscriptions(&mut self) -> Result<(), String> {

        let mut failed_unsubsciptions = Vec::new();

        for (event,uuid) in self.subscriptions.iter() {
            match unsubscribe_evt(event.as_str(), uuid.as_str()).await {
                Ok(_) => println!("Unsubscription to event {} succeded", uuid.as_str()),
                Err(e) =>failed_unsubsciptions.push((event,uuid,e))
            };
        }

        if !failed_unsubsciptions.is_empty() {
            return Err(format!("Failed to unregister {:?}", failed_unsubsciptions))
        }

        Ok(())
    }
}


#[derive(Serialize, Debug)]
struct SubscriptionManifest {
    #[serde(rename = "apiVersion")]
    api_version: String,
    kind: String, 
    metadata: SubscribtionMetadata,
    spec: SubscriptionSpec,
}

#[derive(Serialize, Debug)]
struct SubscribtionMetadata {
    name: String, 
    #[serde(rename = "timeStamp")]
    time_stamp: String,
}

#[derive(Serialize, Debug)]
struct SubscriptionSpec {
    selector: SpecSelector,
    subscriber: SpecSubscriber,
}

#[derive(Serialize, Debug)]
struct SpecSelector {
    #[serde(rename = "matchKind")]
    match_kind: String,
}

#[derive(Serialize, Debug)]
struct SpecSubscriber {
    endpoint: String
}

#[derive(Serialize, Deserialize, Debug)]
struct SubscriptionResponse {
    details: SubscriptionResponseDetails,
}

#[derive(Serialize, Deserialize, Debug)]
struct SubscriptionResponseDetails {
    uuid: String,
}
async fn single_evt_subscription(event: &str) -> Result<String, String> {

    let client = reqwest::Client::new();

    let request_url = String::from("http://127.0.0.1:38368/subscriptions");
    println!("{}", request_url);

    let user_agent = format!("{} - {}",
                                                        option_env!("CARGO_PKG_NAME").unwrap_or("observer"),
                                                        option_env!("CARGO_PKG_VERSION").unwrap_or("unknown"),
                                                    );

     let manifest = SubscriptionManifest {
        api_version: String::from("opentestfactory.org/v1alpha1"),
        kind:  String::from("Subscription"),
        metadata: SubscribtionMetadata { 
            name: String::from(&user_agent), 
            time_stamp: chrono::offset::Local::now().to_rfc3339()
        },
        spec: SubscriptionSpec { 
            selector: SpecSelector { 
                match_kind: String::from(event)
            }, 
            subscriber: SpecSubscriber { 
                endpoint: String::from("http://127.0.0.1:9565/inbox") 
            }
        },
     };

    println!("{:?}", manifest);
    let response = match client
                .post(&request_url)
                .header(USER_AGENT, &user_agent)
                .header(AUTHORIZATION, "Bearer REUSE")
                .json(&manifest).send().await {
        Ok(r) => r,
        Err(e) => return Err(format!("Failed subscribing to event {}. Reason {}",event, e))
    };

    let subscription_resonse_text = match response.status() {
        StatusCode::CREATED =>response.text().await,
        _ =>   return Err(format!("Failed subscribing to event {}. Response {:?}",event, response))
    };

    let subscription  = match subscription_resonse_text {
        Ok(text) =>rocket::serde::json::serde_json::from_str::<SubscriptionResponse>(text.as_str()),
        Err(e) => return Err(format!("Failde to retreive uuid of event {} subscrption: No text content.{}",event, e))
    };
    
    match subscription {
        Ok(sub) =>Ok(sub.details.uuid),
       Err(e) =>return Err(format!("Failde to retreive uuid of event {} subscrption: Parsing error. {}",event, e))
    }
    
}

async fn unsubscribe_evt(event: &str,uuid: &str) -> Result<(), String> {
    println!("Unsubscribing to {} - {}",event, uuid);

    let client = reqwest::Client::new();
    let request_url = format!("http://127.0.0.1:38368/subscriptions/{}", uuid);
    let response = match client
                                                .delete(&request_url)
                                                .header(USER_AGENT, "rimpl Oberser 0.1.0")
                                                .header(AUTHORIZATION, "Bearer REUSE")
                                                .send().await {
        Ok(r) => r,
        Err(e) => return Err(format!("Ouch {}",e))
    };
    println!("{:?}", response);
    println!("{:?}", response.text().await);
    Ok(())
}