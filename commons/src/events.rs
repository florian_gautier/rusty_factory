use std::{ sync::{Arc, RwLock}, collections::{VecDeque}, io, fmt::Debug, fs};

use jsonschema::{JSONSchema};
use rocket::{serde::json::{serde_json::Error, Value}, request::Request, data::{FromData, Outcome}, outcome::Outcome::*, Data, http::{ContentType, Status}};

#[derive(Debug)]
pub struct WorkflowEvent {
    pub event: Value,
}

#[derive(Debug)]
pub enum EventError {
    Io(io::Error),
    TooLarge,
    InvalidJson(Error),
    InvalidEvent(InvalidEventError),
    SchemaValidation(SchemaValidationError)
}

#[derive(Debug)]
pub struct InvalidEventError {
    pub message: String,
    pub content: Value,
}

#[derive(Debug)]
pub struct SchemaValidationError {
    pub errs: Vec<String>,
} 

/* *
 * WorkflowEvent as request data guard.  
 */
#[rocket::async_trait]
impl<'r> FromData<'r> for WorkflowEvent {
    type Error = EventError;


    async fn from_data(req: &'r Request<'_>, data: Data<'r>) -> Outcome<'r, Self>{
        
        let content_type = ContentType::new("application", "json");
        if req.content_type() != Some(&content_type) {
            return Forward(data);
        }

        let limit = req.limits().get("json").unwrap_or(rocket::data::Limits::JSON);

        let string_content = match data.open(limit).into_string().await {
            Ok(string) if string.is_complete() => string.into_inner(),
            Ok(_) => return Failure((Status::PayloadTooLarge, EventError::TooLarge)),
            Err(e) => return Failure((Status::InternalServerError, EventError::Io(e))),
        };

        let data_value = match rocket::serde::json::from_str::<Value>(string_content.as_str()) {
            Ok(v) => v,
            Err(e) => return Failure((Status::BadRequest, EventError::InvalidJson(e))),
        };

        let api_version = match data_value["apiVersion"].as_str() {
            Some(s) => s,
        None => return Failure((Status::BadRequest, EventError::InvalidEvent(InvalidEventError {message: String::from("No API version specified"), content: data_value }))),
        };
        let kind = match data_value["kind"].as_str() {
            Some(s) => s,
            None => return Failure((Status::BadRequest, EventError::InvalidEvent(InvalidEventError {message: String::from("No event kind specified"), content: data_value }))),
        };
    
        let schema_rslt = fs::read_to_string(format!("schemas/{}/{}.json", api_version,kind));
    
        let s_schema = match schema_rslt {
            Ok(s) => s,
            Err(e) => {
                let message = format!("Impossible to load schema: schemas/{}/{}.json. {:?}", api_version,kind, e);
                return Failure((Status::BadRequest, EventError::InvalidEvent(InvalidEventError {message: message, content: data_value })));
            }
        };

        let schema = rocket::serde::json::from_str(&s_schema).expect("A valid schema");

        let compiled_schema = JSONSchema::compile(&schema).expect("A valid schema");

        let validate = compiled_schema.validate(&data_value);
        if let Err(errs) = validate {  
                let mut errors = Vec::new();
                for err in errs {
                    errors.push(format!("{}",err));
                }
                return  Failure((Status::BadRequest, EventError::SchemaValidation(SchemaValidationError {errs: errors})));
        };

        drop(validate);
        return Success(WorkflowEvent {event: data_value})
    }

}

#[derive(Debug)]
pub struct PendingEventQueue {
    pub events : Arc<RwLock<VecDeque<WorkflowEvent>>>,
}


impl PendingEventQueue {
    pub fn new() -> Self { 
        PendingEventQueue {events: Arc::new(RwLock::new(VecDeque::new())) } 
    }
}


/* *
#[derive(Debug, Deserialize,Serialize, PartialEq)]
pub struct WorkflowEvent {
    #[serde(rename = "apiVersion")]
    api_version: String,
    #[serde(flatten)]
    event_variant: EventVariant,

}

#[derive(Debug, Deserialize,Serialize, PartialEq)]
#[serde(tag = "kind")]
pub enum EventVariant {
    #[serde(rename = "Workflow")]
    WORKFLOW {metadata: GenericWorflowEventMetadata},
    #[serde(rename =  "WorkflowCompleted")]
    WORKFLOWCOMPLETED {metadata: GenericWorflowEventMetadata},
    #[serde(rename =  "WorkflowCanceled")]
    WORKFLOWCANCELED {metadata: GenericWorflowEventMetadata},
    #[serde(rename =  "GeneratorCommand")]
    GENERATORCOMMAND {metadata: JobSpecificEventMetadata},
    #[serde(rename =  "GeneratorResult")]
    GENERATORRESULT {metadata: JobSpecificEventMetadata},
    #[serde(rename =  "ProviderCommand")]
    PROVIDERCOMMAND {metadata: ProviderSpecificEventMetadata},
    #[serde(rename =  "ProviderResult")]
    PROVIDERRESULT {metadata: ProviderSpecificEventMetadata},
    #[serde(rename =  "ExecutionCommand")]
    EXECUTIONCOMMAND {metadata: StepSpecificEventMetadata},
    #[serde(rename =  "ExecutionResult")]
    EXECUTIONRESULT {metadata: StepSpecificEventMetadata},
    #[serde(rename =  "ExecutionError")]
    EXECUTIONERROR {metadata: StepSpecificEventMetadata},
    #[serde(rename =  "Notification")]
    NOTIFICATION {metadata: GenericWorflowEventMetadata},
}

#[derive(Debug, Deserialize,Serialize, PartialEq)]
pub struct GenericWorflowEventMetadata {
    name: String, 
    labels: Option<HashMap<String, String>>,
    workflow_id: String,
}
#[derive(Debug, Deserialize,Serialize, PartialEq)]
pub struct JobSpecificEventMetadata {
    name: String, 
    labels: Option<HashMap<String, String>>,
    workflow_id: String,
    job_id: String,
    job_origin: Vec<String>,
}
#[derive(Debug, Deserialize,Serialize, PartialEq)]
pub struct ProviderSpecificEventMetadata {
    name: String, 
    labels: Option<HashMap<String, String>>,
    workflow_id: String,
    job_id: String,
    job_origin: Vec<String>,
    step_id: String,
    step_origin: Option<Vec<String>>,
}
#[derive(Debug, Deserialize,Serialize, PartialEq)]
pub struct StepSpecificEventMetadata {
    name: String, 
    labels: Option<HashMap<String, String>>,
    workflow_id: String,
    job_id: String,
    job_origin: Vec<String>,
    step_id: String,
    step_origin: Option<Vec<String>>,
    step_orgin_satus: Option<HashMap<String, i64>>,
    step_sequence_id: i64,
}

 */